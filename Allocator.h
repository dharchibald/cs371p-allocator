// ----------------------------------
// projects/c++/allocator/Allocator.h
// Copyright (C) 2018
// Glenn P. Downing
// ----------------------------------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument
#include <iostream>
#include <sstream>
#include <cmath>     // abs()

// ---------
// Allocator
// ---------
using namespace std;

template <typename T, size_t N>
class my_allocator {
public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = size_t;
    using difference_type = ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

public:
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

private:
    // ----
    // data
    // ----

    char a[N];

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * Jumps from sentinel to sentinel, seeing if the data all aligns.
     * If a sentinel does not match the previous one, returns false. Otherwise,
     * returns true.
     */
    bool valid () const {

        int index = 0;
        int sval = 0;

        while ((size_t) index < N) {
            int skip = 0;

            sval = (*this)[index];
            if (sval < 0)
                skip = -sval;
            else
                skip = sval;

            index += skip + sizeof(int);
            if ((*this)[index] != sval)
                return false;

            index += sizeof(int);
        }
        return true;
    }



public:

    class iterator {
        friend class my_allocator;

    private:
        const my_allocator& arr;
        int index;

        iterator (const my_allocator& parent, int i) :
            arr (parent),
            index (i)
        {}

    public:
        iterator& operator ++ () {
            int toSkip = arr[index];
            index += abs(toSkip) + 2*sizeof(int);
            return *this;
        }

        pointer operator * () const {
            return (pointer) (arr.a + index + sizeof(int));
        }

        bool operator == (const iterator& rhs) const {
            return (*this).index == rhs.index;
        }

        bool operator != (const iterator& rhs) const {
            return !(*this == rhs);
        }

        bool is_busy () const {
            return (arr[index] < 0);
        }

        const int& sentinel_value () const {
            return (arr[index]);
        }

    };

    // ------------
    // constructors
    // ------------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator () {
        if ( N < sizeof(T) + 2*sizeof(int) ) {
            // throw exception
            throw bad_alloc();
        }

        int index = 0;
        int size = N - 2*sizeof(int);

        // First sentinel
        (*this)[index] = size;
        // Second sentinel
        index = N - sizeof(int);
        (*this)[index] = size;

        assert(valid());
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    iterator begin () const {
        return iterator (*this, 0);
    }

    iterator end () const {
        return iterator (*this, N);
    }


    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     */
    pointer allocate (size_type num) {

        if (num > N - 2*sizeof(int) || num < 0) {
            // throw exception
            throw bad_alloc();
        }

        int bytes_left = 0;
        int data = 0;
        int index = 0;
        bool whole_flag = false;
        bool found = false;

        while (!found && (size_t) index < N) {
            // Get sentinel value
            int sval = (*this)[index];
            // How many bytes we need to allocate
            int need = num * sizeof(T);


            if(sval >= need) {
                // Free block has enough space for data to be allocated
                found = true;

                bytes_left = sval - (need + 2*sizeof(int));
                if (bytes_left <= 0) {
                    // Not enough space to create new free block after replacement
                    // Allocate whole block
                    whole_flag = true;
                    need = sval;
                }

                // Data pointer to the first element in the free block
                data = index + sizeof(int);
                // Create/overwrite sentinel blocks
                (*this)[index] = -need;
                index += need + sizeof(int);
                (*this)[index] = -need;
                index += sizeof(int);


                if (!whole_flag) {
                    // Make two new sentinels for free block
                    (*this)[index] = bytes_left;
                    index += bytes_left + sizeof(int);
                    (*this)[index] = bytes_left;
                    index += sizeof(int);
                }

            } else {
                // Not a free block or not big enough to fit data - skip block
                // sval should be absolute value
                index += abs(sval) + (2*sizeof(int));
            }
        }

        if (!found) {
            // throw exception
            throw bad_alloc();
        }

        assert(valid());
        return reinterpret_cast<pointer>(a+data);
    }

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());
    }                           // from the prohibition of new

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     * <your documentation>
     */
    void deallocate (pointer p, size_type) {
        // Access pointer arithmetic byte by byte
        char* cp = (char*) p;
        if (p == nullptr || cp < a || cp >= a + N) {
            // throw invalid_argument exception
            throw invalid_argument("Wrong");
        }

        int index = int (cp - a);
        int total = 0;
        int sval = 0;

        // Find how much space to free
        index -= sizeof(int);
        sval = (*this)[index];
        total += -sval;

        // Check if left block is free - coalesce if so
        index -= sizeof(int);
        if (index >= 0) {
            sval = (*this)[index];
            if (sval > 0)
                total += (sval + 2*sizeof(int));
        }

        // Check if right block is free - coalesce if so
        index += sizeof(int);
        index += abs((*this)[index]) + (2*sizeof(int));
        if ((size_t) index < N) {
            sval = (*this)[index];
            if (sval > 0) {
                total += (sval + 2*sizeof(int));
                index += (sval + sizeof(int));
            } else {
                index -= sizeof(int);
            }
        } else {
            index -= sizeof(int);
        }

        // Change sentinel values
        (*this)[index] = total;
        index -= (total + sizeof(int));
        (*this)[index] = total;


        assert(valid());
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }

    int& operator [] (char* cp) {
        return *reinterpret_cast<int*>(cp);
    }

    const int& operator [] (char* cp) const {
        return *reinterpret_cast<const int*>(cp);
    }
};



#endif // Allocator_h
