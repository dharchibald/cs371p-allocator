// ---------------------------------------
// projects/c++/allocator/RunAllocator.c++
// Copyright (C) 2018
// Glenn P. Downing
// ---------------------------------------

// --------
// includes
// --------

#include <cassert>  // assert
#include <fstream>  // ifstream
#include <iostream> // cout, endl, getline
#include <string>   // s

#include "Allocator.h"

#define MAX_CASES 100
#define HEAP_SIZE 1000
#define OBJ_SIZE 8

// ----
// main
// ----

using namespace std;

void allocator_read (istream& in, ostream& out) {
    my_allocator<double, HEAP_SIZE> heap;
    string s;
    int action = 0;

    while (getline(in, s) && !s.empty()) {
        istringstream iss(s);
        iss >> action;

        if (action > 0) {
            heap.allocate((size_t) action);
        } else if (action != 0) {
            action = -action;

            int index = 0;
            auto b = begin(heap);
            auto e = end(heap);
            while(index < action && b != e) {
                if(b.is_busy()) {
                    ++index;
                }
                if(index != action) {
                    ++b;
                }
            }

            // deallocate
            if(index == action) {
                heap.deallocate(*b, 0);
            }

        }
    }

    auto b = begin(heap);
    auto e = end(heap);
    while (b != e) {
        out << b.sentinel_value() << " ";
        ++b;
    }
    out << endl;
}

void allocator_eval (istream& in, ostream& out) {
    int cases = 0;
    string s;

    getline(in, s);
    istringstream iss(s);
    iss >> cases;

    assert (0 < cases && cases < MAX_CASES);

    getline(in, s);
    for (int i = 0; i < cases; ++i) {
        allocator_read(in, out);
    }

    return;
}

int main () {

    allocator_eval (cin, cout);

    // read  from stdin,  redirect from RunAllocator.in
    // write to   stdout, redirect to   RunAllocator.out

    return 0;
}
