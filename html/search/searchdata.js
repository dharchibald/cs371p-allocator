var indexSectionsWithContent =
{
  0: "abcdehimoprstv~",
  1: "imt",
  2: "art",
  3: "abcdeimostv~",
  4: "ai",
  5: "acdmprsv",
  6: "mo",
  7: "hmo"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "related",
  7: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Friends",
  7: "Macros"
};

